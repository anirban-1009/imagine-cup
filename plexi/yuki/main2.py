import os
import pygame
pygame.init()
import cv2
import numpy as np
from keras.models import model_from_json
import keras.utils as image


# load model and weight
model = model_from_json(open("fer.json", "r").read())
model.load_weights('fer.h5')
cap = cv2.VideoCapture(0)

face_haar_cascade = cv2.CascadeClassifier('resources/haarcascade_frontalface_default.xml')

SIZE = WIDTH, HEIGHT = 720, 480
BACKGROUND_COLOR = pygame.Color('black')
FPS = 60

screen = pygame.display.set_mode(SIZE)
clock = pygame.time.Clock()


def load_images(path):
    """
    Loads all images in directory. The directory must only contain images.

    Args:
        path: The relative or absolute path to the directory to load images from.

    Returns:
        List of images.
    """
    images = []
    for file_name in os.listdir(path):
        image = pygame.image.load(path + os.sep + file_name).convert()
        images.append(image)
    return images


class AnimatedSprite(pygame.sprite.Sprite):

    def __init__(self, position, images):
        """
        Animated sprite object.

        Args:
            position: x, y coordinate on the screen to place the AnimatedSprite.
            images: Images to use in the animation.
        """
        super(AnimatedSprite, self).__init__()

        size = (32, 32)  # This should match the size of the images.

        self.rect = pygame.Rect(position, size)
        self.images = images
        self.images_right = images
        self.images_left = [pygame.transform.flip(image, True, False) for image in images]  # Flipping every image.
        self.index = 0
        self.image = images[self.index]  # 'image' is the current image of the animation.

        self.velocity = pygame.math.Vector2(0, 0)

        self.animation_time = 0.1
        self.current_time = 0

        self.animation_frames = 6
        self.current_frame = 0

    def update_time_dependent(self, dt):
        """
        Updates the image of Sprite approximately every 0.1 second.

        Args:
            dt: Time elapsed between each frame.
        """
        if self.velocity.x > 0:  # Use the right images if sprite is moving right.
            self.images = self.images_right
        elif self.velocity.x < 0:
            self.images = self.images_left

        self.current_time += dt
        if self.current_time >= self.animation_time:
            self.current_time = 0
            self.index = (self.index + 1) % len(self.images)
            self.image = self.images[self.index]

        self.rect.move_ip(*self.velocity)

    def update_frame_dependent(self):
        """
        Updates the image of Sprite every 6 frame (approximately every 0.1 second if frame rate is 60).
        """
        if self.velocity.x > 0:  # Use the right images if sprite is moving right.
            self.images = self.images_right
        elif self.velocity.x < 0:
            self.images = self.images_left

        self.current_frame += 1
        if self.current_frame >= self.animation_frames:
            self.current_frame = 0
            self.index = (self.index + 1) % len(self.images)
            self.image = self.images[self.index]

        self.rect.move_ip(*self.velocity)

    def update(self, dt):
        """This is the method that's being called when 'all_sprites.update(dt)' is called."""
        # Switch between the two update methods by commenting/uncommenting.
        self.update_time_dependent(dt)
        # self.update_frame_dependent()

class YukiSim():
    def __init__(self):
        self.neutral_img = pygame.image.load("resources/neutral.png")
        joy_img = load_images(path="resources/happy")
        self.joy_sequence = AnimatedSprite((0, 0), joy_img)
        self.joy_sprites = pygame.sprite.Group(self.joy_sequence)
        
    def update(self, state, dt):
        if state == 'neutral':
            screen.blit(self.neutral_img, (0, 0))
            pygame.display.flip()
        elif state == 'angry':
            print("Angry")
        elif state == "happy":
            print("happy")
            self.joy_sprites.update(dt)
            screen.fill(BACKGROUND_COLOR)
            self.joy_sprites.draw(screen)


def main():
    running = True
    yuki = YukiSim()

    while running:
        dt = clock.tick(60) / 1000

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        if cv2.waitKey(10) == ord('q'):
            running = False


        ret, test_img = cap.read() # captures frame and returns boolean value and captured image
        if not ret:
            continue
        gray_img = cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)

        faces_detected = face_haar_cascade.detectMultiScale(gray_img, 1.32, 5)
        predicted_emotion = "neutral"

        for (x, y, w, h) in faces_detected:
            cv2.rectangle(test_img, (x, y), (x+w, y+h), (255, 0, 0), thickness=7)
            roi_gray = gray_img[y:y+w, x:x+h] # cropping region of interest i.e. face area from  image
            roi_gray = cv2.resize(roi_gray, (48, 48))
            img_pixels = image.img_to_array(roi_gray)
            img_pixels = np.expand_dims(img_pixels, axis=0)
            img_pixels /= 255

            predictions = model.predict(img_pixels)

            # find max indexed array
            max_index = np.argmax(predictions[0])

            emotions = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
            predicted_emotion = emotions[max_index]

            cv2.putText(test_img, predicted_emotion, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)

        yuki.update(predicted_emotion, dt)
        resized_img = cv2.resize(test_img, (480, 320))
        cv2.imshow('Facial emotion analysis ', resized_img)

        screen.fill(BACKGROUND_COLOR)
        pygame.display.update()


if __name__ == "__main__":
    main()

    cap.release()
    cv2.destroyAllWindows()
